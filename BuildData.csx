#r "nuget:Newtonsoft.Json"
//#load "nuget:Newtonsoft.Json, 12.0.3"
//#load "Choco.csx"

using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json;

class JsonDataContent
{
    public string project;
    public string title;
    public string content;
    public DateTime create;
    public DateTime modified;
}

public static void Run()
{
    Func<string, string, GroupCollection[]> regPattern = (string data, string pattern) => {
        MatchCollection matches = Regex.Matches(data, pattern, RegexOptions.IgnoreCase);
        if (matches == null || matches.Count == 0) return null;

        GroupCollection[] gg = new GroupCollection[matches.Count];
        for (int i = 0; i < matches.Count; i++)
        {
            Match match = matches[i];
            // Console.WriteLine("Length:" + match.Groups.Count);
            // Console.WriteLine("A:" + match.Groups[0].Value);
            // Console.WriteLine("B:" + match.Groups[1].Value);
            gg[i] = match.Groups;
        }
        
        return gg;
    };

    
    
    Console.WriteLine("find data files");

    List<JsonDataContent> listData = new List<JsonDataContent>();

    string[] files = Directory.GetFiles("./Data");
    foreach(var i in files)
    {
        if (i.IndexOf(".html") == -1) continue;

        string content = File.ReadAllText(i);

        Func<string, string, string> getData = (string pattern, string data) => {
            string ret = "";

            GroupCollection[] groups = null;
            groups = regPattern(content, pattern);
            if(groups != null)
            {
                foreach(var d in groups)
                {
                    //Console.WriteLine(d["content"].ToString());
                    ret += d[data].ToString();
                }
            }

            return ret;
        };

        string strDateCreate = getData(@"<create>(?<create>.*?)</create>", "create");
        string strDateModified = getData(@"<modified>(?<modified>.*?)</modified>", "modified");


	DateTime creation = File.GetCreationTime(i);
        DateTime lastModified = File.GetLastWriteTime(i);
	creation = TimeZoneInfo.ConvertTimeToUtc(creation);
	lastModified = TimeZoneInfo.ConvertTimeToUtc(lastModified);



	DateTime dateCreate = lastModified;//DateTime.UtcNow;
	DateTime dateModified = lastModified;//DateTime.UtcNow;


	if (strDateCreate != null && strDateCreate != "")
	{
		//dateCreate = DateTime.SpecifyKind(DateTime.ParseExact(strDateCreate, "yyyy-MM-ddTHH:mm:sszzz", CultureInfo.InvariantCulture), DateTimeKind.Utc);
		dateCreate = DateTime.ParseExact(strDateCreate, "yyyy-MM-ddTHH:mm:sszzz", CultureInfo.InvariantCulture);
		dateCreate = TimeZoneInfo.ConvertTimeToUtc(dateCreate);
		//DateTimeOffset dateOffset = new DateTimeOffset(dateCreate, DateTimeOffset.Now.Offset);
	}

	if (strDateModified != null && strDateModified != "")
	{
		//dateModified = DateTime.SpecifyKind(DateTime.ParseExact(strDateModified, "yyyy-MM-ddTHH:mm:sszzz", CultureInfo.InvariantCulture), DateTimeKind.Utc);
		dateModified = DateTime.ParseExact(strDateModified, "yyyy-MM-ddTHH:mm:sszzz", CultureInfo.InvariantCulture);
		dateModified = TimeZoneInfo.ConvertTimeToUtc(dateModified);
	}

	//Console.WriteLine("test convert : " + TimeZoneInfo.ConvertTimeToUtc(DateTime.UtcNow));
	//Console.WriteLine(i + " > dateCreate " + strDateCreate);
	//Console.WriteLine(i + " > dateModified " + strDateModified);

	//Console.WriteLine(dateCreate);
	//Console.WriteLine(dateModified);


        string dataProject = getData(@"<project>(?<project>.*?)</project>", "project");


        string dataTitle = getData(@"<title>(?<title>.*?)</title>", "title");
        //Console.WriteLine("dataTitle : " + dataTitle);

        string dataContent = getData(@"<content>(?<content>[\s\S]*?)</content>", "content");
        //Console.WriteLine("dataContent : " + dataContent);

        if (dataTitle.Length == 0 && dataContent.Length == 0) continue;

        Console.WriteLine(">" + i);
        

        listData.Add( new JsonDataContent(){ project = dataProject, title = dataTitle, content = dataContent, create = dateCreate, modified = dateModified, } );
    }
    
    listData = listData.OrderByDescending(x => x.modified).ToList();

	foreach(var i in listData)
	{
		Console.WriteLine(i.title);
		Console.WriteLine("--write : " + i.modified);
	}

    //Console.WriteLine(JsonConvert.SerializeObject(listData));

    //string dataJsonPath = "./Data/contents.json";
    string dataJsonPath = "./wwwroot/contents.json";
    File.WriteAllText(dataJsonPath, JsonConvert.SerializeObject(listData));
    Console.WriteLine($"write data {dataJsonPath}");
}


Run();


