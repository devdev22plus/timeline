FILE_SAVE=./Data/writeNow.xhtml

if test -f "$FILE_SAVE"; then
	if [ "$1" ]; then
		dateNow=$(date +"%y")-$(date +"%m")-$(date +"%d")
		fileName="$dateNow-$1.html"

		fileSavePath="./Data/$fileName"

		if test -f "$fileSavePath"; then
			read -p "file destination is exists make sure do you want to save overwrite Y/n : " makesure

			if [ $makesure == "Y" ] || [ $makesure == "y" ]; then
				echo "save overwriting"
			else
				exit 1
			fi
		fi

		mv $FILE_SAVE $fileSavePath
		#cp $FILE_SAVE $fileSavePath

		#dateData=$(date +"%s")
		dateData=$(date -u +"%Y-%m-%dT%H:%M:%S%z")

		printf "<modified>$dateData</modified>\r\n\r\n" | cat - $fileSavePath > temp && mv temp $fileSavePath

		#echo $fileName
		echo "Save data file to $fileSavePath"
	else
		echo "please enter something about save file"
	fi
else
	echo "not found file on writing please write something first"
fi


